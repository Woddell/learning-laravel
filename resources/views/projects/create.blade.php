<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Create</title>
</head>
<body>


  <h1>Create a new project</h1>

  <form action="/projects" method="post">
    {{ csrf_field() }}
    <div>
      <input type="text" name="title" required placeholder="Project title">
    </div>
    <div>
      <textarea name="description" id="" cols="30" rows="10" required placeholder="Project description"></textarea>
    </div>
    <div>
      <button type="submit">Create project</button>
    </div>

    @if ($errors->any())
    <div>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    </div>
    @endif
  </form>


</body>
</html>