@extends('layout')

@section('content')
<div>
  <h1>{{ $project->title }}</h1>
  <p>{{ $project->description}}</p>
</div>

<div>
    <p>
    <a href="/projects/{{$project->id}}/edit">Edit</a>
    </p>
  </div>

@if ($project->tasks->count())
<div>
    @foreach ($project->tasks as $task)
      <li>
        <form action="/tasks/{{ $task->id }}" method="post">
          @method('patch')
          @csrf
          <label for="completed">
            <input type="checkbox" name="completed" onchange="this.form.submit()" {{ $task->completed ? 'checked' : ''}}>
            {{ $task->description }}
          </label>
        </form>
      </li>
    @endforeach
  </div>
@endif

<form action="">
  <div>
    <label for="">New Task</label>
    <input type="text" name="" placeholder="">
  </div>
</form>



@endsection