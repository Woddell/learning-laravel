@extends('layout')

@section('content')
<h3>Edit Project</h3>

<form method="POST" action="/projects/{{ $project->id }}">
  @method('PATCH')
  @csrf


  <div>
    <input type="text" name="title" placeholder="Title" value="{{ $project->title }}">
  </div>
  <div>
  <textarea name="description" placeholder="Description" cols="30" rows="10">{{ $project->description }}</textarea>
  </div>
  <button type="submit">Update Project</button>
</form>
<form action="/projects/{{ $project->id }}" method="POST" >
  @method('DELETE')
  @csrf
  <button type="submit">Delete Project</button>
</form>

@endsection