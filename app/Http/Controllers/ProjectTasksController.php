<?php

namespace App\Http\Controllers;

use App\Task;

class ProjectTasksController extends Controller
{

    public function store(Project $project)
    {
        $project->addTask(
            request()->validation(['description' => 'required'])
        );

        return back();
    }

    public function update(Task $task)
    {
        $task->complete(request()->has('completed'));

        return back();
    }
}
